-- Starting Out

-- Notes in comments

-- To change prompt use
-- :set prompt "ghci  >"

-- If we want to use negative numbers, its always best to surround it with
-- paranthesis
-- > 5 * -3  will give you an error
-- correct
x = 5 * (-3)

-- for equality the operators are == for equality and /= for not equals

-- succ function takes anything that has a defined successor and returns that
-- successor.
y = succ 8

-- min and max takes two things can be put in order and returns one lesser or
-- one greater out of the two.
a = min 9 10
b = min 3.4 3.2
c = max 100 101

-- Function application has the highest precedence of all

-- If a function takes two parameters we call always call it as an infix function
-- by surrounding it with backticks. e.g div funciton for integral division.

-- first function
doubleMe :: Num a => a -> a
doubleMe y = y + y

doubleUs :: Num a => a -> a -> a
doubleUs x y = x*2 + y*2

-- functions in Haskell don't have to be in any particular order, so it doesn't
-- matter if we define functions first or later.

-- function that multiplies a number by 2 but only if that number is smaller
-- than or equal to 100
doubleSmallNumber x = if x > 100
                        then x
                        else x * 2

-- The difference between Haskell's if statement and if statement in imperative
-- languages is that the else part is mandatory in Haskell.
-- If statement in Haskell is an expression. An expression is basically a piece
-- of code that returns a value. Because the else is mandatory, an If statement
-- will always return something, that's why its an expression.

doubleSmallNumber' x = (if x > 100 then x else x * 2) + 1


-- In Haskell lists are a homogeneous data structures. It stores several elements
-- of the same type.

-- Strings are just a list of characters.
-- eg. "Hello" is just syntactic sugar for ['H', 'e', 'l', 'l', 'o']

-- list concatenation is done by ++
st = "hello" ++ " " ++ "world"

-- [1,2,3] is just syntactic sugar for 1:2:3:[]

-- cycle takes a list and cycles it into an infinite list
d = take 10 (cycle [1,2,3])

-- List comprehension
--
-- Find first 10 even numbers
e = [x * 2 | x <- [1..10]]

-- Find double of first 10 numbers, such that the double value is greater
-- than 12
f = [x * 2 | x <- [1..10], 2 * x >= 12]

-- All numbers from 50 to 100, whose remainder when divided by the number
-- 7 is 3
g = [x | x <- [50..100], x `mod` 7 == 3]

-- Function to replace each odd number less than 10 with "BOOM!"
-- and each odd number greater than 10 by "BANG!"
boomBangs xs = [if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]

-- all number from 10 to 20 that are not 13, 15 or 19
h = [x | x <- [10..20], x/=13, x/=15, x/=19]

-- Product of all possible combination of two lists
i = [x * y | x<-[2,5,10], y<-[8,10,11]]

-- All products in the previous list greater than 50
j = [x * y | x<-[2,5,10], y<-[8,10,11], x*y > 50]

-- Our own version of the length function, to find the length of a list
length' xs = sum [1| _ <-xs]

-- Function that takes a string and removes everything except
-- uppercase letters from it
removeNonUppercase st = [ c | c<-st, c `elem` ['A'..'Z']]

-- Example of nested list comprehensions
xxs = [[1,3,5,2,3,1,2,4,5], [1,2,3,4,5,6,7,8,9], [1,2,4,2,1,6,3,1,3,2,3,6]]

k = [ [x | x<-xs, even x] | xs <- xxs]

-- Demonstrating the zip function
l = zip [1,2,3,4,5] [5,5,5,5,5]

m = zip [1..5] ["one", "two", "three", "four", "five"]

-- which right angled triangle, that has integer for all sides, and all sides are equal to or smaller than 10
-- and has a perimeter of 24
n = [ (a,b,c) | c<-[1..10], b<-[1..c], a<-[1..b], a^2 + b^2 == c^2, a+b+c==24]

