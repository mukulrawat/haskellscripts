-- Higher order functions
--
-- Haskell functions can take functions as parameters and return functions
-- as return values. A function that does either of that is called a higher
-- order function.
--

-- curried functions

-- a simple curried function
multThree :: (Num a) => a -> a -> a -> a
multThree = \x -> (\y -> (\z -> x*y*z))

-- a curried function that takes a number and compare it to 100
compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred = compare 100

-- a function made using sections
divideByTen :: (Floating a) => a -> a
divideByTen = (/10)

-- a function that checks if the character supplied to it is in upper case
isUpperAlphanum :: Char -> Bool
isUpperAlphanum = (`elem` ['A'..'Z'])


-- the applytwice function, applies same function to the argument twice
applyTwice :: (a -> a) -> a -> a
applyTwice = \f -> (\x -> f(f x))

-- our implementation of the zipwith function
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y: zipWith' f xs ys

-- our own implementation of the flip function
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' = \f -> (\x -> (\y -> f y x))

-- our implementation of the map function
map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = f x : map' f xs

-- To find the largest number under 100,000 that's divisible by 3829
largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000, 99999..])
  where p x = x `mod` 3829 == 0

-- find the sum of all odd squares that are smaller than 10,000
res1 = sum (takeWhile (<10000) (filter odd (map (^2) [1..])))

-- same result using a list comprehension
res2 = sum (takeWhile (<10000) [n^2 | n <- [1..], odd (n^2)])

-- for all starting numbers between 1 and 100, how many collatz
-- chains have a length greater than 15
chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
  | even n = n : chain (n `div` 2)
  | odd n  = n : chain (3*n + 1)

numLongChains :: Int
numLongChains = length (filter (\xs -> length xs > 15) (map chain [1..100]))

-- Example of lambda functions using zipWith and map
res3 = zipWith (\a b -> (a * 30 + 3) / b) [5,4,3,2,1] [1,2,3,4,5]

res4 = map (\(a,b) -> a + b) [(1,2), (3,5), (6,3), (2,6), (2,5)]

-- example of a function written normally and then written lambda functions
-- to illustrate currying
addThree :: (Num a) => a -> a -> a -> a
addThree x y z = x + y + z

addThree' :: (Num a) => a -> a -> a -> a
addThree' = \x -> (\y -> (\z -> x + y + z))

-- The higher order foldr function

-- defining sum and product using foldr
sum', product' :: (Num a) => [a] -> a
sum' = foldr (+) 0
product' = foldr (*) 1

-- defining and and or operations using foldr
and', or' :: [Bool] -> Bool
and' = foldr (&&) True
or' = foldr (||) False

-- defining concat function using foldr
concat' :: [[a]] -> [a]
concat' = foldr (++) []

-- defining length function using foldr
length' :: [a] -> Int
length' = foldr (\_ xs -> 1 + xs) 0

-- defining reverse function using foldr
reverse' :: [a] -> [a]
reverse' = foldr ((++) . ( \x xs -> xs ++ [x])) []

-- defining map and filter with foldr
map'':: (a -> b) -> [a] -> [b]
map'' f = foldr ((:) . f) []

filter'' :: (a -> Bool) -> [a] -> [a]
filter'' pred = foldr ((++) . (\x -> if pred x then [x] else [])) []

-- The higher-order scanr function
--
-- defining the tails function which return all the tail segments of
-- a list in decreasing order of length
tails :: [a] -> [[a]]
tails [] = [[]]
tails xs = [xs] ++ tails (tail xs)

-- The function scanr applies foldr to every tail segment of a list
-- beginning with the longest

-- Higer order function foldr1
--
-- using foldr to find the maximum element of a list
maxList = foldr1 max

-- higher order function foldl

-- some functions defined using foldl
and'', or'' :: [Bool] -> Bool
and'' = foldl (&&) True
or'' = foldl (&&) False

sum'', product'' :: Num a => [a] -> a
sum'' = foldl (+) 0
product'' = foldl (*) 1

concat'' :: [[a]] -> [a]
concat'' (++) []

length'' :: [a] -> Int
length'' = foldl (\x _ -> x + 1) 0

reverse'' :: [a] -> [a]
reverse'' = foldl (flip (:)) []

-- The higher order scanl function

-- inits function that returns all initial segments of a list in
-- increasing order of length, starting with the empty list

inits :: [a] -> [[a]]
inits [] = [[]]
inits xs = inits (init xs) ++ [xs]

-- factorial of a list can be computed as
res5 = scanl (*) 1 [2..100]


