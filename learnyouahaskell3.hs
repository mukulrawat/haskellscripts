-- list comprehension that filters a string so that only caps remain
removeNonUppercase :: [Char] -> [Char]
removeNonUppercase st = [ c | c<-st, c `elem` ['A'..'Z']]

-- A function that takes 3 integers and add them together
addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

-- factorial function using Integers
factorial :: Integer -> Integer
factorial n = product [1..n]

-- circumference function using float
circumference :: Float -> Float
circumference r = 2 * pi * r

-- circumference function using double
circumference' :: Double -> Double
circumference' r = 2 * pi * r


