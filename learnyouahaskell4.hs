-- Example of pattern matching
-- trivial function that checks if the number supplied is 7 or not
lucky :: (Integral a) => a -> String
lucky 7  = "Lucky number seven!"
lucky _  = "Sorry, you're out of luck pal!"

-- trivial function that says the number from 1 to 5
sayMe :: (Integral a ) => a -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe _ = "Not between 1 and 5!"

-- Factorial function defined recursively
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n-1)

-- Pattern matching on tuples
-- Function to add two vectors
-- without using pattern matching
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors a b = (fst a + fst b, snd a + snd b)

-- using pattern matching
addVectors' :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors' (x1, y1) (x2, y2) = (x1+x2, y1+y2)

-- Extracting componenets in a triple using pattern matching
first :: (a, b, c) -> a
first (x, _, _) = a

second :: (a, b, c) -> b
second (_, y, _) = y

third :: (a, b, c) -> c
third (_, _, z) = z

-- pattern matching in a list expression
xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]
xss = [a+b, | (a,b) <- xs]

-- our own implemntation of the head function
head' :: [a] -> a
head' [] = error "Can't call head on an empty list, dummy!"
head' (x:_) = x

-- our own implementation of the length function
length' :: (Num b) => [a] -> b
length' [] = 0
length' (_: xs) = 1 + length' xs

-- our own implementation of the sum function
sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

-- Patterns are a handy way of breaking something up according to a pattern and
-- binding it to names whilst still keeping a reference to the whole thing
capital :: String -> String
capital "" = "Empty string, whoops!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

-- our own implementation of the max function
max' :: (Ord a) => a -> a -> a
max' a b
  | a > b = a
  |otherwise = b

-- our own implementation of the compare function
myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
  | a > b     = GT
  | a == b     = EQ
  | otherwise = LT

-- trivial function where we get a first and a last name and return initials
initials :: String -> String -> String
initials firstname lastname = [f] ++ ". " ++ [l] ++ "."
  where (f: _) = firstname
        (l: _) = lastname

-- function that gives us cylinder's surface area based on height and radius
cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
  let sideArea = 2 * pi * r * h
      topArea = pi * r^2
  in sideArea + 2 * topArea

-- difference between let and where is that let bindings are expressions
-- themselves where bindings are just syntactic constructs.

calcBmis :: (RealFloat a) => [(a,a)] -> [a]
calcBmis xs = [bmi | (w,h) <- xs, let bmi = w/h^2, bmi >=25.0]

-- Another implementation of head function using case expressions
head'' :: [a] -> a
head'' xs = case xs of [] -> error "No head for empty lists!"
                       (x:_) -> x

-- case expressions can be used anywhere
describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of [] -> "empty."
                                              [x] -> "a singleton list."
                                              xs -> "a longer list."


