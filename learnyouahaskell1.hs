-- Notes go over here in the comments

-- Haskell is a purely functional programming language.
-- In imperative programming languages, we get things done by giving the
-- computer a sequence of instructions. While executing them it can change
-- state.

-- In purely functional programming language you don't tell the computer
-- what to do but we tell it what stuff is.
-- e.g. Factorial = product of all the numbers from 1 to that number
-- sum of a list of numbers is the first number plus the sum of all other
-- numbers.

-- We can't set a variable to something and then change it later.

-- So in purely functional programming language a function has no side-effects.
-- The only thing a function does is calculate something and return the result.

-- If a function is called twice with the same parameters, its gauranteed to
-- return the same result. That's called referential transparency.
-- Referential transparency simply means that an expression always evaluate to
-- the same result in any context.
-- In imperative programming languages, side effects like imperative update
-- break this desirable property.

-- Haskell is lazy, meaning unless specifically told otherwise, Haskell won't
-- execute functions and calculate things.
-- Programs are simply a series of transformations on data.
-- Lazy evaluation allows us to have infinite data structures.

-- Haskell is statically typed. When compiling a program Haskell knows, which
-- piece of code is a number, or a string and so on.
-- Haskell uses a type system that has type inference.
-- Type inference is a feature of the type system which means that concrete
-- types are deduced by the type system whereever it is obvious.

-- Haskell is elegant and concise.




