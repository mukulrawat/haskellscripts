import Data.Char (toUpper)

s = "Hello"

-- example of list comprehension
a = [toUpper c | c <- s]

-- or use map
b = map toUpper s

-- multiple generators separated by commas
c = [(i,j) | i<-[1,2] , j<-[1..4]]

-- another example where the second list is infinite
d =  take 10 [ (i,j) | i<-[1,2] , j<-[1..]]

-- a nested sequence of list comprehension
e = take 5 [ [(i,j) | i<-[1,2]] | j<-[1..]]

-- one can also provide boolean guards
f = take 10 [ (i,j) | i<-[1..]. j<-[1..i-1]. gcd i j = 1]

-- one can also make local let declarations
g = take 10 [ (i,j) | i<-[1..], let k = i*i, j <- [1..k]]



